package task11.enums;

public enum Species {
    Dog,
    Cat,
    Fish,
    DomesticCat,
    RoboCat,
    UNKNOWN
}
