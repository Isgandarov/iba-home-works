package task11;

import task11.Family;
import task11.Human;
import task11.dao.CollectionFamilyDao;
import task11.enums.DayOfWeek;
import task11.pets.Pet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class FamilyService {
    private CollectionFamilyDao dao = new CollectionFamilyDao();
    private Family fam;
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

    public List<Family> getAllFamilies() {
        return dao.getAllFamilies();
    }

    public void displayAllFamilies() {

        dao.getAllFamilies().stream().forEach(fam -> System.out.println(fam));
    }

    List<Family> getFamiliesBiggerThan(int count) {

        return dao.getAllFamilies().stream().filter(el -> el.getCountOfMembers() > count).collect(Collectors.toList());

    }

    List<Family> getFamiliesLessThan(int count) {
        return dao.getAllFamilies().stream().filter(el -> el.getCountOfMembers() < count).collect(Collectors.toList());
    }

    void createNewFamily(Human man, Human woman) {
        fam = new Family(man, woman);
        dao.saveFamily(fam);
    }


    public Family getFamilyByIndex(int index) {
        return dao.getFamilyByIndex(index);
    }

    void deleteFamilyByIndex(int index) {
        dao.deleteFamily(index);
    }

    Family bornChild(Family family, String gender) throws ParseException {
        Human newChild;
        if (gender.equals("masculine")) {
            newChild = new Human("Jordan", family.getFather().getSurname(), 80, format.parse("13/06/2008"), new HashMap<Enum, String>() {{
                put(DayOfWeek.SATURDAY, "swimming");
                put(DayOfWeek.MONDAY, "cooking");
            }});

        } else
            newChild = new Human("Kyle", family.getFather().getSurname(), 80, format.parse("28/12/2010"), new HashMap<Enum, String>() {{
                put(DayOfWeek.SATURDAY, "swimming");
                put(DayOfWeek.MONDAY, "cooking");
            }});

        family.addChild(newChild);
        dao.saveFamily(family);

        return family;
    }

    Family adoptChild(Family family, Human human) {
        family.getChildren().add(human);
        return family;
    }

    void deleteAllChildrenOlderThan(int age) {
     /*   for (Family el : dao.getAllFamilies()) {
            List<Human> list = el.getChildren();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getAge() > age)
                    el.getChildren().remove(i);
            }
            dao.saveFamily(el);
        }*/
        dao.getAllFamilies().stream().flatMap(fam -> fam.getChildren().stream().filter(child -> child.getAge() > age).map(child -> fam.getChildren().remove(child)));
        dao.getAllFamilies().forEach(fam -> dao.saveFamily(fam));
    }

    int getCountOfFamilies() {
        return getAllFamilies().size();
    }

    int getCountOfFamiliesWithMemberNumber(int count) {

        return (int) dao.getAllFamilies().stream().filter(fam -> fam.getCountOfMembers() == count).count();

    }

    Family getFamilyById(int id) {
        return dao.getFamilyByIndex(id);
    }

    List<Pet> getPets(int index) {
        return dao.getAllFamilies().get(index).getPets();
    }

    void addPet(int index, Pet pet) {
        Family fam = dao.getAllFamilies().get(index);
        fam.addPet(pet);
        dao.saveFamily(fam);
    }

}
