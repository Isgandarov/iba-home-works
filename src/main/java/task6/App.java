package task6;

class App {
   public static void main(String[] args) {


       final int maxLimitation = 5;
       Human child[] = new Human[maxLimitation];
       Human mother = new Human("Anna", "Morty", 110, 1982, new String[][]{{DayOfWeek.SATURDAY.name(), "swimming"}, {DayOfWeek.MONDAY.name(), "cooking"}});
       Human father = new Human("John", "Morty", 110, 1981, new String[][]{{DayOfWeek.SUNDAY.name(), "eating"}, {DayOfWeek.WEDNESDAY.name(), "working"}});
       Pet pet = new Pet(Species.DOG, "Max", new String[]{"eat", "bark"}, 6, 28);
       Family family = new Family(father, mother, pet);
       child[0] = new Human("Tom", "Morty", 110, 2001, new String[][]{{DayOfWeek.SUNDAY.name(), "sleeping"}, {DayOfWeek.TUESDAY.name(), "reading"}});
       child[1] = new Human("Bob", "Morty", 110, 2001, new String[][]{{DayOfWeek.TUESDAY.name(), "singing"}, {DayOfWeek.SATURDAY.name(), "hangout with friends"}});
       child[2] = new Human("Charlie", "A", 110, 2001, new String[][]{{DayOfWeek.MONDAY.name(), "eating"}, {DayOfWeek.THURSDAY.name(), "watching TV"}});


       for (Human obj : child) {
           if (father.equals(obj))
               family.addChild(obj);
       }

       System.out.println(family);
       System.out.println("Count of members: "+family.getCountOfMembers());


       family.deleteChild(child[1]);
       garbageCollector();



   }
   public static void garbageCollector(){
       for (int i = 0; i <10000000 ; i++) {
           Human human = new Human("John", "Morty", 110, 1981, new String[][]{{DayOfWeek.SUNDAY.name(), "eating"}, {DayOfWeek.WEDNESDAY.name(), "working"}});
           System.gc();
       }
   }
}
