package task6;

import java.util.ArrayList;
import java.util.Arrays;

class Family {
    private Human father, mother;
    private ArrayList<Human> children = new ArrayList<>();
    private Pet pet;
    private int countOfMembers = 2;

    public Family(Human father, Human mother, Pet pet) {
        this.father = father;
        this.mother = mother;
        this.pet = pet;

    }

    public Human getFather() {
        return father;
    }

    public Human getMother() {
        return mother;
    }

    private String[] children_names() {
        String[] s = new String[getCountOfMembers() - 2];
        for (int i = 0; i < countOfMembers - 2; i++) {
            s[i] = children.get(i).getName();
        }
        return s;
    }


    public Pet getPet() {
        return pet;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }


    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void addChild(Human child) {
        children.add(child);
        countOfMembers++;
    }

    public boolean deleteChild(int index) {
        if (children.size() == 0 || index > children.size()) return false;


        children.remove(index);



        countOfMembers--;

        return true;


    }
    public boolean deleteChild(Human child) {
        if (!children.contains(child)) return false;



        children.remove(child);



        countOfMembers--;

        return true;


    }



    public int getCountOfMembers() {

        return countOfMembers;
    }

    @Override
    public String toString() {
        return String.format("The %ss' family{father:%s, mother:%s, children:{%s} , pet:%s }", father.getSurname(), father, mother, Arrays.toString(children.toArray()), pet);
    }


}
