package task6;

import java.util.Arrays;

class Human {
    private String name;
    private String surname;
    private String[][] schedule = new String[7][2];
    private int iq;
    private int year;


    public Human(String name, String surname, int iq, int year, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.iq = iq;
        this.year = year;
        this.schedule = schedule;


    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public String toString() {

        return String.format("Human{name=%s, surname=%s, year=%d, iq=%d, schedule=%s", name, surname, year, iq, Arrays.deepToString(schedule));
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (!(that instanceof Human)) return false;

        Human human = (Human) that;
        return (human.surname.equals(this.surname));
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Successfully deleted");
    }


}
