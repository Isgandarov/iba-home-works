package task4;

import java.util.Random;

 class Human {
    private String name, surname, pet, mother, father;
    private String[][] schedule;
    private int IQ, year;

    public Human(String name, String surname, String pet, String mother, String father, int IQ, int year) {
        this.name = name;
        this.surname = surname;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.IQ = IQ;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void greetPet() {
        System.out.println("Hello, " + this.pet);
    }

    public void describePet(String spec, int years) {
        System.out.printf("I have a %s he is %d years old.He is lazy.\n", spec, years);
    }

    @Override
    public String toString() {
        return String.format("Human{name=%s, surname=%s, year=%d, iq=%d, mother=%s %s, father=%s %s,pet= ", name, surname, year, IQ, mother, surname, father, surname);
    }

    public void feedPet(int lvl, String perName, String spec) {
        Random rand = new Random();
        int num = rand.nextInt(100) + 1;
        if (num < lvl) System.out.printf("Hm.. I will feed %s's %s\n", perName, spec);
        else System.out.printf("I think %s's %s is not hungry\n", perName, spec);
    }


}

