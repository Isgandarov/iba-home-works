package task4;

import java.util.Arrays;

public class Pet {
    private String specie;
    private String nickname;
    private String[] habits;
    private int age;
    private int trickLevel;

    public Pet(String specie, String nickname, String[] habits, int age, int trickLevel) {
        this.specie = specie;
        this.nickname = nickname;
        this.habits = habits;
        this.age = age;
        this.trickLevel = trickLevel;
    }

    public int getAge() {
        return age;
    }

    public String getSpecie() {
        return specie;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void eat() {
        System.out.println("I am eating");
    }

    public void respond() {
        System.out.println("Hello owner. I am " + this.specie + ". I miss you ");
    }

    public void foul() {
        System.out.println("I need to cover it up");
    }

    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}", specie, nickname, age, trickLevel, Arrays.toString(habits));

    }


}
