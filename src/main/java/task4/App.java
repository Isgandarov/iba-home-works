package task4;

public class App {
    public static void main(String[] args) {
        Pet pt1 = new Pet("dog", "Shepherd", new String[]{"barking", "sleeping"}, 5, 24);
        Human per1 = new Human("Jax", "Teller", pt1.getSpecie(), "Gemma", "John", 120, 1990);
        ;
        Pet pt2 = new Pet("cat", "Violet", new String[]{"eating", "disturbing"}, 3, 50);
        Human per2 = new Human("Bob", "Harley", pt1.getSpecie(), "Jasie", "Mike", 100, 1997);


        String petInfos1 = pt1.toString();
        System.out.println(pt1.toString());
        System.out.println(per1.toString() + petInfos1);

        pt1.respond();
        pt1.eat();
        pt1.foul();

        per1.describePet(pt1.getSpecie(), pt1.getAge());
        per1.feedPet(pt1.getTrickLevel(), per1.getName(), pt1.getSpecie());


        System.out.println(pt2);
        System.out.println(per2 +" "+ pt2);

        pt2.respond();
        pt2.eat();
        pt2.foul();

        per2.describePet(pt2.getSpecie(), pt2.getAge());
        per2.feedPet(pt2.getTrickLevel(), per2.getName(), pt2.getSpecie());


    }
}
