package task3;

import java.util.Scanner;

public class TaskPlanner {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[][] schedule = new String[7][2];
        String input;

        fillMatrix(schedule);

        boolean flag = true;
        while (flag) {
            System.out.print("Please input the day of the week: ");
            input = sc.nextLine().toLowerCase();

            switch (input) {
                case "monday":
                    System.out.printf("Your task for Monday: %s\n", schedule[1][1]);
                    break;
                case "tuesday":
                    System.out.printf("Your task for Tuesday: %s\n", schedule[2][1]);
                    break;
                case "wednesday":
                    System.out.printf("Your task for Wednesday: %s\n", schedule[3][1]);
                    break;
                case "thursday":
                    System.out.printf("Your task for Thursday: %s\n", schedule[4][1]);
                    break;
                case "friday":
                    System.out.printf("Your task Friday : %s\n", schedule[5][1]);
                    break;
                case "saturday":
                    System.out.printf("Your task for Saturday: %s\n", schedule[6][1]);
                    break;
                case "sunday":
                    System.out.printf("Your task for Sunday: %s\n", schedule[0][1]);
                    break;
                case "change monday":
                    System.out.print("Please, input new tasks for Monday: ");
                    schedule[1][1] = sc.nextLine();
                    break;
                case "change tuesday":
                    System.out.print("Please, input new tasks for Tuesday: ");
                    schedule[2][1] = sc.nextLine();
                    break;
                case "change wednesday":
                    System.out.print("Please, input new tasks for Wednesday: ");
                    schedule[3][1] = sc.nextLine();
                    break;
                case "change thursday":
                    System.out.print("Please, input new tasks for Thrusday: ");
                    schedule[4][1] = sc.nextLine();
                    break;
                case "change friday":
                    System.out.print("Please, input new tasks for Friday: ");
                    schedule[5][1] = sc.nextLine();
                    break;
                case "change saturday":
                    System.out.print("Please, input new tasks for Saturday: ");
                    schedule[6][1] = sc.nextLine();
                    break;
                case "change sunday":
                    System.out.print("Please, input new tasks for Sunday: ");
                    schedule[0][1] = sc.nextLine();
                    break;
                case "exit":
                    flag = false;
                    break;
                default:
                    System.out.print("Sorry, I don't understand you, please try again.");
                    break;

            }

        }
    }

    private static void fillMatrix(String[][] schedule) {
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to course and watch film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go to gym and rest";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "go to dentist";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "do some practices";
        schedule[5][0] = "Friday";
        schedule[5][1] = "go to course and write cod";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "hangout with friends";
    }
}
