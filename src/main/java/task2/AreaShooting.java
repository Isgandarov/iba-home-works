package task2;

import java.util.Random;
import java.util.Scanner;

public class AreaShooting {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char[][] matrix = new char[5][5];
        make_area(matrix);

        Random rand = new Random();
        int r1 = rand.nextInt(5);
        int r2 = rand.nextInt(5);


        int line, shooting;

        while (true) {
            System.out.print("Enter line: ");
            line = sc.nextInt();

            if (line > 5 || line <= 0) {
                System.out.println("Please enter number in 1-5 range");
                continue;
            }
            System.out.print("Enter shooting: ");
            shooting = sc.nextInt();
            if (shooting > 5 || shooting <= 0) {
                System.out.println("Please enter number in 1-5 range");
                continue;
            }

            if (line - 1 == r1 && shooting - 1 == r2) {
                updated_area(matrix, line, shooting, true);
                System.out.println("Congrats!You have won");
                break;
            } else updated_area(matrix, line, shooting, false);


        }

    }

    private static void make_area(char[][] matrix) {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                matrix[i][j] = '-';
            }
        }
    }

    private static void updated_area(char[][] matrix, int line, int shooting, boolean checker) {
        if (checker)
            matrix[line - 1][shooting - 1] = 'x';
        else matrix[line - 1][shooting - 1] = '*';


        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {

                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }

    }
}


