package task8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

class App {
    public static void main(String[] args) {


        ArrayList<Human> children = new ArrayList<>();
        Human mother = new Human("Anna", "Morty", 110, 1982, new HashMap<Enum, String>() {{
            put(DayOfWeek.Saturday, "swimming");
            put(DayOfWeek.Monday, "cooking");
        }});
        Human father = new Human("John", "Morty", 110, 1981, new HashMap<Enum, String>() {{
            put(DayOfWeek.Sunday, "eating");
            put(DayOfWeek.Wednesday, "working");
        }});
        Pet pet = new Pet(Species.Dog, "Max", new HashSet<String>(Arrays.asList("eating,sleeping")), 6, 28);
        Family family = new Family(father, mother, pet);
        children.add(new Human("Tom", "Morty", 110, 2001, new HashMap<Enum, String>() {{
            put(DayOfWeek.Sunday, "sleeping");
            put(DayOfWeek.Tuesday, "reading");
        }}));
        children.add(new Human("Bob", "Morty", 100, 2003, new HashMap<Enum, String>() {{
            put(DayOfWeek.Wednesday, "singing");
            put(DayOfWeek.Saturday, "playing");
        }}));
        children.add(new Human("Charlie", "A", 111, 2006, new HashMap<Enum, String>() {{
            put(DayOfWeek.Monday, "learning");
            put(DayOfWeek.Thursday, "sleeping");
        }}));


        for (Human obj : children) {
            if (father.equals(obj))
                family.addChild(obj);
        }

        System.out.println(family);
        System.out.println("Count of members: " + family.getCountOfMembers());


        family.deleteChild(children.get(0));


    }

}
