package task8;

enum Species {
    Dog,
    Cat,
    Fish,
    DomesticCat,
    RoboCat,
    UNKNOWN
}
