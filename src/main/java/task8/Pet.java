package task8;

import java.util.Arrays;
import java.util.Set;

class Pet {
    private Species specie;
    private String nickname;
    private Set<String> habits;
    private int age;
    private int trickLevel;

    public Pet(Species specie, String nickname, Set<String> habits, int age, int trickLevel) {
        this.specie = specie;
        this.nickname = nickname;
        this.habits = habits;
        this.age = age;
        this.trickLevel = trickLevel;
    }

    public int getAge() {
        return age;
    }

    public Species getSpecie() {
        return specie;
    }

    public int getTrickLevel() {
        return trickLevel;
    }


    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}", specie, nickname, age, trickLevel, habits);

    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Successfully deleted");
    }


}
