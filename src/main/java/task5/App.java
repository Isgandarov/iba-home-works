package task5;

 class App {
    public static void main(String[] args) {

        Human child[] = new Human[5];
        Human mother = new Human("Anna", "Morty", 110, 1982, new String[][]{{"Monday", "swimming"}, {"Saturday", "cooking"}});
        Human father = new Human("John", "Morty", 110, 1981, new String[][]{{"Tuesday", "eating"}, {"Thursday", "working"}});
        Pet pet = new Pet("dog", "Maks", new String[]{"eat", "bark"}, 6, 28);
        Family family = new Family(father, mother, pet);
        child[0] = new Human("Tom", "Morty", 110, 2001, new String[][]{{"Wednesday", "sleeping"}, {"Monday", "reading"}});
        child[1] = new Human("Bob", "Morty", 110, 2001, new String[][]{{"Monday", "singing"}, {"Friday", "hangout with friends"}});
        child[2] = new Human("Charlie", "A", 110, 2001, new String[][]{{"Sunday", "eating"}, {"Wednesday", "watching TV"}});

        for (Human obj : child) {
            if (father.equals(obj))
                family.addChild(obj);
        }

        System.out.println(family);
        System.out.println("Count of members: "+family.getCountOfMembers());


        family.deleteChild(1);

        System.out.println(family);
        System.out.println("Count of members after deleting child: "+family.getCountOfMembers());


    }
}
