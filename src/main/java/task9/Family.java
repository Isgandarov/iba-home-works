package task9;


import java.util.*;

class Family {
    private Human father, mother;
    private List<Human> children = new ArrayList<>();
    private List<Pet> pets = new ArrayList<>();
    private int countOfMembers = 2;

    public Family(Human father, Human mother) {
        this.father = father;
        this.mother = mother;

    }

    public Human getFather() {
        return father;
    }

    public Human getMother() {
        return mother;
    }

    private String[] children_names() {
        String[] s = new String[getCountOfMembers() - 2];
        for (int i = 0; i < countOfMembers - 2; i++) {
            s[i] = children.get(i).getName();
        }
        return s;
    }


    public void setFather(Human father) {
        this.father = father;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }


    // public void setPet(Pet pet) {this.pet = (List<Pet>) pet;}

    public void addChild(Human child) {
        children.add(child);
        countOfMembers++;
    }


    public boolean deleteChild(Human child) {
        if (!children.contains(child)) return false;


        children.remove(child);


        countOfMembers--;

        return true;


    }


    public int getCountOfMembers() {

        return countOfMembers;
    }

    @Override
    public String toString() {
        return String.format("The %ss' family{father:%s, mother:%s, children:%s , pet:%s }", father.getSurname(), father, mother, children, pets);
    }


    public void addPet(Pet pet) {
        pets.add(pet);
    }

    public List<Pet> getPets() {
        return pets;
    }


    public List<Human> getChildren() {
        return children;
    }
}
