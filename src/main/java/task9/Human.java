package task9;


import java.util.Map;

class Human {
    private String name;
    private String surname;
    private Map<Enum,String> schedule;
    private int IQ;
    private int year;
    private int age;



    public Human(String name, String surname, int IQ, int year, Map<Enum,String> schedule) {
        this.name = name;
        this.surname = surname;
        this.IQ = IQ;
        this.year = year;
        this.schedule = schedule;


    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public String toString() {

        return String.format("Human{name=%s, surname=%s, year=%d, iq=%d, schedule=%s", name, surname, year, IQ, schedule);
    }
    public int getAge() {
        return 2019-year;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (!(that instanceof Human)) return false;

        Human human = (Human) that;
        return (human.surname.equals(this.surname));
    }




}
