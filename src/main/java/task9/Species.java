package task9;

enum Species {
    Dog,
    Cat,
    Fish,
    DomesticCat,
    RoboCat,
    UNKNOWN
}
