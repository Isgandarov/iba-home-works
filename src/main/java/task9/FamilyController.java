package task9;

import java.util.ArrayList;
import java.util.List;

class FamilyController {
    private FamilyService service = new FamilyService();

    List<Family> getAllFamilies() {
        return service.getAllFamilies();
    }

    void displayAllFamilies() {
        service.displayAllFamilies();
    }

    List<Family> getFamiliesBiggerThan(int count) {

        return service.getFamiliesBiggerThan(count);
    }

    List<Family> getFamiliesLessThan(int count) {

        return service.getFamiliesLessThan(count);
    }

    void createNewFamily(Human man, Human woman) {
        service.createNewFamily(man, woman);
    }


    void deleteFamilyByIndex(int index) {
        service.deleteFamilyByIndex(index);
    }

    Family bornChild(Family family, String gender) {
        return service.bornChild(family,gender);
    }

    Family adoptChild(Family family, Human human) {
        return service.adoptChild(family,human);
    }

    void deleteAllChildrenOlderThan(int age) {
     service.deleteAllChildrenOlderThan(age);
    }

    int getCountOfFamilies() {

        return service.getCountOfFamilies();
    }

    Family getFamilyById(int id) {

        return service.getFamilyById(id);
    }

    List<Pet> getPets(int index) {

        return service.getPets(index);
    }

    int getCountOfFamiliesWithMemberNumber(int num){

        return service.getCountOfFamiliesWithMemberNumber(num);
    }

    void addPet(int index, Pet pet) {
      service.addPet(index,pet);
    }
}
