package task9;

import java.util.ArrayList;
import java.util.List;

class CollectionFamilyDao implements FamilyDao {
    List<Family> allFamily = new ArrayList<>();
    @Override
    public List<Family> getAllFamilies() {
        return allFamily;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return allFamily.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if(index>=allFamily.size()) return  false;
        allFamily.remove(index);
        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if(!allFamily.contains(family)) return false;
        allFamily.remove(family);
        return true;
    }

    @Override
    public void saveFamily(Family family) {
        if(allFamily.contains(family)) {
            allFamily.set(allFamily.indexOf(family),family);
        }
        else allFamily.add(family);

    }
}
