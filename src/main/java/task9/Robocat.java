package task9;

import java.util.ArrayList;


class Robocat extends Pet {
    private Species specie;
    private String nickname;
    private ArrayList<String> habits;
    private int age;
    private int trickLevel;

    public Robocat(Species specie, String nickname, ArrayList<String> habits, int age, int trickLevel) {
        this.specie = specie;
        this.nickname = nickname;
        this.habits = habits;
        this.age = age;
        this.trickLevel = trickLevel;
    }

    public int getAge() {
        return age;
    }

    public Species getSpecie() {
        return specie;
    }

    public int getTrickLevel() {
        return trickLevel;
    }


    @Override
    public String toString() {
        return String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}", specie, nickname, age, trickLevel, habits);

    }

    @Override
    void eat() {
        System.out.println("I am eating battery");
    }

    @Override
    void respond() {
        System.out.println("I am the smartest cat");
    }

}
