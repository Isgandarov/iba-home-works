package task9;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class FamilyService {
    CollectionFamilyDao dao = new CollectionFamilyDao();
    Family fam;

    List<Family> getAllFamilies() {
        return dao.getAllFamilies();
    }

    void displayAllFamilies() {
        for (Family el : dao.getAllFamilies()) {
            System.out.println(el);
        }
    }

    List<Family> getFamiliesBiggerThan(int count) {
        List<Family> list = new ArrayList<>();
        for (Family el : dao.getAllFamilies()) {
            if (el.getCountOfMembers() > count)
                list.add(el);
        }
        return list;
    }

    List<Family> getFamiliesLessThan(int count) {
        List<Family> list = new ArrayList<>();
        for (Family el : dao.getAllFamilies()) {
            if (el.getCountOfMembers() < count)
                list.add(el);
        }
        return list;
    }

    void createNewFamily(Human man, Human woman) {
        fam = new Family(man, woman);
        dao.saveFamily(fam);
    }


    public Family getFamilyByIndex(int index) {
        return dao.getFamilyByIndex(index);
    }

    void deleteFamilyByIndex(int index) {
        dao.deleteFamily(index);
    }

    Family bornChild(Family family, String gender) {
        Human newChild;
        if (gender.equals("masculine")) {
            newChild = new Human("Jordan", family.getFather().getSurname(), 80, 2008, new HashMap<Enum, String>() {{
                put(DayOfWeek.Saturday, "swimming");
                put(DayOfWeek.Monday, "cooking");
            }});

        } else newChild = new Human("Kyle", family.getFather().getSurname(), 80, 2008, new HashMap<Enum, String>() {{
            put(DayOfWeek.Saturday, "swimming");
            put(DayOfWeek.Monday, "cooking");
        }});

        family.addChild(newChild);
        dao.saveFamily(family);

        return family;
    }

    Family adoptChild(Family family, Human human) {
        family.getChildren().add(human);
        return family;
    }

    void deleteAllChildrenOlderThan(int age) {
        for (Family el : dao.getAllFamilies()) {
            List<Human> list = el.getChildren();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getAge() > age)
                    el.getChildren().remove(i);
            }
            dao.saveFamily(el);
        }
    }

    int getCountOfFamilies() {
        return getAllFamilies().size();
    }

    int getCountOfFamiliesWithMemberNumber(int count) {
        int c = 0;
        for (Family el : dao.getAllFamilies()) {
            if (el.getCountOfMembers() == count) c++;
        }
        return c;


    }

    Family getFamilyById(int id) {
        return dao.getFamilyByIndex(id);
    }

    List<Pet> getPets(int index) {
        return dao.getAllFamilies().get(index).getPets();
    }

    void addPet(int index, Pet pet) {
        Family fam = dao.getAllFamilies().get(index);
        fam.addPet(pet);
        dao.saveFamily(fam);
    }

}
