package task9;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class App {
    public static void main(String[] args) {
        FamilyController controller = new FamilyController();

        Human woman1 = new Human("Anna", "Roosvelt", 110, 1982, new HashMap<Enum, String>() {{
            put(DayOfWeek.Saturday, "swimming");
            put(DayOfWeek.Monday, "cooking");
        }});
        Human man1 = new Human("John", "Morty", 110, 1981, new HashMap<Enum, String>() {{
            put(DayOfWeek.Sunday, "eating");
            put(DayOfWeek.Wednesday, "working");

        }});

        Human woman2 = new Human("Lyla", "Watson", 110, 1982, new HashMap<Enum, String>() {{
            put(DayOfWeek.Saturday, "swimming");
            put(DayOfWeek.Monday, "cooking");
        }});
        Human man2 = new Human("Mike", "Sparrow", 115, 1983, new HashMap<Enum, String>() {{
            put(DayOfWeek.Sunday, "eating");
            put(DayOfWeek.Wednesday, "working");

        }});
        Human woman3 = new Human("Maria", "La", 118, 1990, new HashMap<Enum, String>() {{
            put(DayOfWeek.Saturday, "swimming");
            put(DayOfWeek.Monday, "cooking");
        }});
        Human man3 = new Human("Chris", "Martin", 130, 1983, new HashMap<Enum, String>() {{
            put(DayOfWeek.Sunday, "eating");
            put(DayOfWeek.Wednesday, "singing");

        }});
        Human adoption = new Human("Za", "Ja", 110, 2008, new HashMap<Enum, String>() {{
            put(DayOfWeek.Saturday, "swimming");
            put(DayOfWeek.Monday, "cooking");
        }});
        Pet dog1 = new Dog(Species.Dog, "Maks", new ArrayList<>(Arrays.asList("eat", "bark")) , 6, 80);
        Pet cat1 = new Cat(Species.Cat, "Tom", new ArrayList<>(Arrays.asList("sleep", "spoil")), 3, 50);
        Pet roboCat1 = new Robocat(Species.RoboCat, "BoJack", new ArrayList<>(Arrays.asList("run", "sleep")), 10, 15);
        Pet dog2 = new Dog(Species.Dog, "Aaa", new ArrayList<>(Arrays.asList("eat", "bark")), 6, 80);
        Pet cat2 = new Cat(Species.Cat, "Bbb", new ArrayList<>(Arrays.asList("sleep", "spoil")), 3, 50);
        Pet roboCat2 = new Robocat(Species.RoboCat, "Ccc", new ArrayList<>(Arrays.asList("run", "sleep")), 10, 15);


        controller.createNewFamily(man1, woman1);
        controller.createNewFamily(man2, woman2);
        controller.createNewFamily(man3, woman3);
        System.out.println("Display all families");
        controller.displayAllFamilies();
        System.out.println("\n");

        controller.bornChild(controller.getFamilyById(0), "feminine");
        controller.bornChild(controller.getFamilyById(1), "feminine");
        controller.adoptChild(controller.getFamilyById(2), adoption);
        controller.bornChild(controller.getFamilyById(1), "masculine");

        System.out.println("Bigger than specified number:");
        System.out.println(controller.getFamiliesBiggerThan(3));
        System.out.println("\n");

        System.out.println("Less than specified number:");
        System.out.println(controller.getFamiliesLessThan(3));
        System.out.println("\n");

        System.out.println("Get family by id:");
        System.out.println(controller.getFamilyById(1));
        System.out.println("\n");

        System.out.println("Count of families in the list:");
        System.out.println(controller.getCountOfFamilies());
        System.out.println("\n");

        controller.addPet(0, dog1);
        controller.addPet(0, roboCat1);
        controller.addPet(1, cat1);
        controller.addPet(2, dog1);
        controller.addPet(2, cat2);

        System.out.println("Display families after addition child(ren):");
        controller.displayAllFamilies();
        System.out.println("\n");

        System.out.println("Count of families with specified number:");
        System.out.println(controller.getCountOfFamiliesWithMemberNumber(3));
        System.out.println("\n");

        System.out.println("Get pets of specified family:");
        System.out.println(controller.getPets(0));
        System.out.println("\n");


        controller.deleteFamilyByIndex(1);
        System.out.println("Display families after deleting some of them:");
        controller.displayAllFamilies();


    }

}
