package task10;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

class Human {
    private String name;
    private String surname;
    private Map<Enum, String> schedule;
    private int iq;
    private Date birthDate;
    private int age;


    public Human(String name, String surname, int iq, Date birthDate, Map<Enum, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.iq = iq;
        this.birthDate = birthDate;
        this.schedule = schedule;


    }

    public String getName() {

        return name;
    }

    public String getSurname() {

        return surname;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String date = formatter.format(birthDate);

        return String.format("Human{name=%s, surname=%s, birthDate=%s, iq=%d, schedule=%s", name, surname, date, iq, schedule);
    }

    public int getAge() {
        int b = birthDate.getYear();
        return 2019 - b;
    }
    public String describeAge(){
        LocalDate lc = LocalDate.now();

        return String.format("Year:%d Month:%d Day:%d",lc.getYear()-birthDate.getYear(),lc.getMonthValue()-birthDate.getMonth(),lc.getDayOfMonth()-birthDate.getDay());
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (!(that instanceof Human)) return false;

        Human human = (Human) that;
        return (human.surname.equals(this.surname));
    }


}
