package task7;

import java.util.Arrays;

public class Man extends Human {
    private String name;
    private String surname;
    private String[][] schedule = new String[7][2];
    private int IQ;
    private int year;


    public Man(String name, String surname, int IQ, int year, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.IQ = IQ;
        this.year = year;
        this.schedule = schedule;


    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    void repairCar(){
        System.out.println("The motor of my car goes down.I need repair my car.");
    }

    @Override
    public String toString() {

        return String.format("Man{name=%s, surname=%s, year=%d, iq=%d, schedule=%s", name, surname, year, IQ, Arrays.deepToString(schedule));
    }

    @Override
    void greetPet() {
        System.out.println("Hello my dear pet");
    }
}
