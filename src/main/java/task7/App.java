package task7;

import task6.DayOfWeek;


class App {
    public static void main(String[] args) {


        Human woman = new Woman("Anna", "Morty", 110, 1982, new String[][]{{DayOfWeek.SATURDAY.name(), "swimming"}, {DayOfWeek.MONDAY.name(), "cooking"}});
        Human man = new Man("John", "Morty", 110, 1981, new String[][]{{DayOfWeek.SUNDAY.name(), "eating"}, {DayOfWeek.WEDNESDAY.name(), "working"}});
        Pet dog = new Dog(Species.Dog, "Maks", new String[]{"eat", "bark"}, 6, 80);
        Pet cat = new Cat(Species.Cat, "Tom", new String[]{"sleep", "spoil"}, 3, 50);
        Pet roboCat = new RoboCat(Species.RoboCat, "BoJack", new String[]{"run", "sleep"}, 10, 15);

        System.out.println(woman);
        woman.greetPet();
        ((Woman) woman).makeup();

        System.out.println(man);
        man.greetPet();
        ((Man) man).repairCar();

        System.out.println(dog);
        dog.eat();
        ((Dog) dog).foul();
        dog.respond();

        System.out.println(cat);
        cat.eat();
        ((Cat) cat).foul();
        cat.respond();

        System.out.println(roboCat);
        roboCat.eat();
        roboCat.respond();



    }
}
