package task7;

import java.util.Arrays;

  class Woman extends Human {
    private String name;
    private String surname;
    private String[][] schedule = new String[7][2];
    private int IQ;
    private int year;


    public Woman(String name, String surname, int IQ, int year, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.IQ = IQ;
        this.year = year;
        this.schedule = schedule;


    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void makeup(){
        System.out.println("I love to decorate myself");
    }

    @Override
    public String toString() {

        return String.format("Woman{name=%s, surname=%s, year=%d, iq=%d, schedule=%s", name, surname, year, IQ, Arrays.deepToString(schedule));
    }

    @Override
    void greetPet() {
        System.out.println("Come here darling");
    }
}
