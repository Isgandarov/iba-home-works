package task9;


import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static org.junit.Assert.*;


public class FamilyServiceTest {

    FamilyController controller = new FamilyController();

    Human woman1 = new Human("Anna", "Roosvelt", 110, 1982, new HashMap<Enum, String>() {{
        put(DayOfWeek.Saturday, "swimming");
        put(DayOfWeek.Monday, "cooking");
    }});
    Human man1 = new Human("John", "Morty", 110, 1981, new HashMap<Enum, String>() {{
        put(DayOfWeek.Sunday, "eating");
        put(DayOfWeek.Wednesday, "working");

    }});

    Human woman2 = new Human("Lyla", "Watson", 110, 1982, new HashMap<Enum, String>() {{
        put(DayOfWeek.Saturday, "swimming");
        put(DayOfWeek.Monday, "cooking");
    }});
    Human man2 = new Human("Mike", "Sparrow", 115, 1983, new HashMap<Enum, String>() {{
        put(DayOfWeek.Sunday, "eating");
        put(DayOfWeek.Wednesday, "working");

    }});
    Human woman3 = new Human("Maria", "La", 118, 1990, new HashMap<Enum, String>() {{
        put(DayOfWeek.Saturday, "swimming");
        put(DayOfWeek.Monday, "cooking");
    }});
    Human man3 = new Human("Chris", "Martin", 130, 1983, new HashMap<Enum, String>() {{
        put(DayOfWeek.Sunday, "eating");
        put(DayOfWeek.Wednesday, "singing");

    }});
    Human adoption = new Human("Za", "Ja", 110, 2008, new HashMap<Enum, String>() {{
        put(DayOfWeek.Saturday, "swimming");
        put(DayOfWeek.Monday, "cooking");
    }});
    Pet dog1 = new Dog(Species.Dog, "Maks", new ArrayList<>(Arrays.asList("eat", "bark")) , 6, 80);
    Pet cat1 = new Cat(Species.Cat, "Tom", new ArrayList<>(Arrays.asList("sleep", "spoil")), 3, 50);
    Pet horse1 = new Robocat(Species.RoboCat, "BoJack", new ArrayList<>(Arrays.asList("run", "sleep")), 10, 15);
    Pet dog2 = new Dog(Species.Dog, "Aaa", new ArrayList<>(Arrays.asList("eat", "bark")), 6, 80);
    Pet cat2 = new Cat(Species.Cat, "Bbb", new ArrayList<>(Arrays.asList("sleep", "spoil")), 3, 50);
    Pet horse2 = new Robocat(Species.RoboCat, "Ccc", new ArrayList<>(Arrays.asList("run", "sleep")), 10, 15);



    @Test
    public void getFamilyByIndex() {
        controller.createNewFamily(man1,woman1);
        controller.createNewFamily(man2,woman2);
        controller.createNewFamily(man3,woman3);
        assertEquals("The Sparrows' family{father:Human{name=Mike, surname=Sparrow, year=1983, iq=115, schedule={WEDNESDAY=working, SUNDAY=eating}, mother:Human{name=Lyla, surname=Watson, year=1982, iq=110, schedule={SATURDAY=swimming, MONDAY=cooking}, children:[] , pet:[] }",controller.getFamilyById(1).toString());




    }

    @Test
    public void getCountOfFamilies() {
        controller.createNewFamily(man1,woman1);
        controller.createNewFamily(man2,woman2);
        controller.createNewFamily(man3,woman3);
        assertEquals(3,controller.getCountOfFamilies());
    }

    @Test
    public void getCountOfFamiliesWithMemberNumber() {
        controller.createNewFamily(man1,woman1);
        controller.createNewFamily(man2,woman2);
        controller.createNewFamily(man3,woman3);
        controller.bornChild(controller.getFamilyById(0), "feminine");
        controller.bornChild(controller.getFamilyById(1), "feminine");
        controller.adoptChild(controller.getFamilyById(2), adoption);
        controller.bornChild(controller.getFamilyById(1), "masculine");
        assertEquals(1,controller.getCountOfFamiliesWithMemberNumber(3));
    }



    @Test
    public void getPets() {
        controller.createNewFamily(man1,woman1);
        controller.createNewFamily(man2,woman2);
        controller.createNewFamily(man3,woman3);
        controller.addPet(0, dog1);
        controller.addPet(0, horse2);
        assertEquals("[DOG{nickname='Maks', age=6, trickLevel=80, habits=[eat, bark]}, HORSE{nickname='Ccc', age=10, trickLevel=15, habits=[run, sleep]}]",controller.getPets(0).toString());

    }
}
