package task6;


import org.junit.Test;


import static org.junit.Assert.*;


public class FamilyTest {


    @Test
    public void addChildTest1() {
        final int maxLimitation = 5;
        Human child[] = new Human[maxLimitation];
        Human mother = new Human("Anna", "Morty", 110, 1982, new String[][]{{DayOfWeek.SATURDAY.name(), "swimming"}, {DayOfWeek.MONDAY.name(), "cooking"}});
        Human father = new Human("John", "Morty", 110, 1981, new String[][]{{DayOfWeek.SUNDAY.name(), "eating"}, {DayOfWeek.WEDNESDAY.name(), "working"}});
        Pet pet = new Pet(Species.DOG, "Maks", new String[]{"eat", "bark"}, 6, 28);
        Family family = new Family(father, mother, pet);
        child[0] = new Human("Tom", "Morty", 110, 2001, new String[][]{{DayOfWeek.SUNDAY.name(), "sleeping"}, {DayOfWeek.TUESDAY.name(), "reading"}});

        assertTrue(father.equals(child[0]));
    }

    @Test
    public void addChildTest2() {
        final int maxLimitation = 5;
        Human child[] = new Human[maxLimitation];
        Human mother = new Human("Anna", "Morty", 110, 1982, new String[][]{{DayOfWeek.SATURDAY.name(), "swimming"}, {DayOfWeek.MONDAY.name(), "cooking"}});
        Human father = new Human("John", "Morty", 110, 1981, new String[][]{{DayOfWeek.SUNDAY.name(), "eating"}, {DayOfWeek.WEDNESDAY.name(), "working"}});
        Pet pet = new Pet(Species.DOG, "Maks", new String[]{"eat", "bark"}, 6, 28);
        Family family = new Family(father, mother, pet);
        child[0] = new Human("Tom", "John", 110, 2001, new String[][]{{DayOfWeek.SUNDAY.name(), "sleeping"}, {DayOfWeek.TUESDAY.name(), "reading"}});
        assertFalse(father.equals(child[0]));
    }

    @Test
    public void deleteChildTest1() {
        final int maxLimitation = 5;
        Human child[] = new Human[maxLimitation];
        Human mother = new Human("Anna", "Morty", 110, 1982, new String[][]{{DayOfWeek.SATURDAY.name(), "swimming"}, {DayOfWeek.MONDAY.name(), "cooking"}});
        Human father = new Human("John", "Morty", 110, 1981, new String[][]{{DayOfWeek.SUNDAY.name(), "eating"}, {DayOfWeek.WEDNESDAY.name(), "working"}});
        Pet pet = new Pet(Species.DOG, "Maks", new String[]{"eat", "bark"}, 6, 28);
        Family family = new Family(father, mother, pet);
        child[0] = new Human("Tom", "Morty", 110, 2001, new String[][]{{DayOfWeek.SUNDAY.name(), "sleeping"}, {DayOfWeek.TUESDAY.name(), "reading"}});
        family.addChild(child[0]);
        assertTrue(family.deleteChild(child[0]));
    }

    @Test
    public void testDeleteChild2() {
        final int maxLimitation = 5;
        Human child[] = new Human[maxLimitation];
        Human mother = new Human("Anna", "Morty", 110, 1982, new String[][]{{DayOfWeek.SATURDAY.name(), "swimming"}, {DayOfWeek.MONDAY.name(), "cooking"}});
        Human father = new Human("John", "Morty", 110, 1981, new String[][]{{DayOfWeek.SUNDAY.name(), "eating"}, {DayOfWeek.WEDNESDAY.name(), "working"}});
        Pet pet = new Pet(Species.DOG, "Maks", new String[]{"eat", "bark"}, 6, 28);
        Family family = new Family(father, mother, pet);
        child[0] = new Human("Tom", "Morty", 110, 2001, new String[][]{{DayOfWeek.SUNDAY.name(), "sleeping"}, {DayOfWeek.TUESDAY.name(), "reading"}});
        assertFalse(family.deleteChild(child[3]));
    }
}
