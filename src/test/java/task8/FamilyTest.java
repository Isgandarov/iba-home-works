package task8;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;


public class FamilyTest {


    @Test
    public void addChildTest1() {

        ArrayList<Human> children = new ArrayList<>();
        Human mother = new Human("Anna", "Morty", 110, 1982, new HashMap<Enum, String>() {{
            put(DayOfWeek.Saturday, "swimming");
            put(DayOfWeek.Monday, "cooking");
        }});
        Human father = new Human("John", "Morty", 110, 1981, new HashMap<Enum, String>() {{
            put(DayOfWeek.Sunday, "eating");
            put(DayOfWeek.Wednesday, "working");
        }});
        Pet pet = new Pet(Species.Dog, "Max", new HashSet<String>(Arrays.asList("eating,sleeping")), 6, 28);
        Family family = new Family(father, mother, pet);
        children.add(new Human("Tom", "Morty", 110, 2001, new HashMap<Enum, String>() {{
            put(DayOfWeek.Sunday, "sleeping");
            put(DayOfWeek.Tuesday, "reading");
        }}));
        assertTrue(father.equals(children.get(0)));

    }

    @Test
    public void addChildTest2() {
        ArrayList<Human> children = new ArrayList<>();
        Human mother = new Human("Anna", "Morty", 110, 1982, new HashMap<Enum, String>() {{
            put(DayOfWeek.Saturday, "swimming");
            put(DayOfWeek.Monday, "cooking");
        }});
        Human father = new Human("John", "Morty", 110, 1981, new HashMap<Enum, String>() {{
            put(DayOfWeek.Sunday, "eating");
            put(DayOfWeek.Wednesday, "working");
        }});
        Pet pet = new Pet(Species.Dog, "Max", new HashSet<String>(Arrays.asList("eating,sleeping")), 6, 28);
        Family family = new Family(father, mother, pet);
        children.add(new Human("Tom", "Cruze", 110, 2001, new HashMap<Enum, String>() {{
            put(DayOfWeek.Sunday, "sleeping");
            put(DayOfWeek.Tuesday, "reading");
        }}));
        assertFalse(father.equals(children.get(0)));

    }

    @Test
    public void deleteChildTest2() {
        ArrayList<Human> children = new ArrayList<>();
        Human mother = new Human("Anna", "Morty", 110, 1982, new HashMap<Enum, String>() {{
            put(DayOfWeek.Saturday, "swimming");
            put(DayOfWeek.Monday, "cooking");
        }});
        Human father = new Human("John", "Morty", 110, 1981, new HashMap<Enum, String>() {{
            put(DayOfWeek.Sunday, "eating");
            put(DayOfWeek.Wednesday, "working");
        }});
        Pet pet = new Pet(Species.Dog, "Max", new HashSet<String>(Arrays.asList("eating,sleeping")), 6, 28);
        Family family = new Family(father, mother, pet);
        children.add(new Human("Tom", "Morty", 110, 2001, new HashMap<Enum, String>() {{
            put(DayOfWeek.Sunday, "sleeping");
            put(DayOfWeek.Tuesday, "reading");
        }}));
        family.addChild(children.get(0));
        assertTrue(family.deleteChild(0));

    }

    @Test
    public void deleteChildTest1() {
        ArrayList<Human> children = new ArrayList<>();
        Human mother = new Human("Anna", "Morty", 110, 1982, new HashMap<Enum, String>() {{
            put(DayOfWeek.Saturday, "swimming");
            put(DayOfWeek.Monday, "cooking");
        }});
        Human father = new Human("John", "Morty", 110, 1981, new HashMap<Enum, String>() {{
            put(DayOfWeek.Sunday, "eating");
            put(DayOfWeek.Wednesday, "working");
        }});
        Pet pet = new Pet(Species.Dog, "Max", new HashSet<String>(Arrays.asList("eating,sleeping")), 6, 28);
        Family family = new Family(father, mother, pet);
        children.add(new Human("Tom", "Morty", 110, 2001, new HashMap<Enum, String>() {{
            put(DayOfWeek.Sunday, "sleeping");
            put(DayOfWeek.Tuesday, "reading");
        }}));
        assertFalse(family.deleteChild(10));

    }
}
